#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Simple Bot to reply to Telegram messages
# This program is dedicated to the public domain under the CC0 license.

"""
Bot for CrowdPotato to reply to consumers on Telegram. 
"""

from telegram import ReplyKeyboardMarkup
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)
from twilio.rest import Client

import requests
import logging


# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

CHOOSING, TYPING_REPLY, TYPING_CHOICE = range(3)

reply_keyboard = [['Order Food', 'Check Delivery Status'],
                  ['Cancel Order', 'Fufill Order', 'Promotions'],
                  ['Done', 'Help']]
markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)

def facts_to_str(user_data):
    facts = list()

    for key, value in user_data.items():
        facts.append('{} - {}'.format(key, value))

    return "\n".join(facts).join(['\n', '\n'])


def start(bot, update):
    if update.message["chat"]["username"] is None:
        update.message.reply_text("Please make a username before using the service!")
    else:
        string = "Hello " + str(update.message["chat"]["username"]) + ", how can I help you today?"
        update.message.reply_text(
            string,
            reply_markup=markup)
        print(CHOOSING)
        return CHOOSING



def regular_choice(bot, update, user_data):
    text = update.message.text
    user_data[str(update.message["chat"]["id"])] = {}
    print("Setting choice...")
    user_data[str(update.message["chat"]["id"])]['topic'] = text

    selection = user_data[str(update.message["chat"]["id"])]['topic']
    print("User selection " + selection)

    if selection == "Order Food":
        r = requests.get("https://limitless-harbor-23385.herokuapp.com/stores")
        json = r.json()
        stores = json["data"]
        string = ""

        for store in stores:
            string = string + "\n " + str(store["store_id"]) + ".\t" + str(store["store_name"])


        update.message.reply_text("Absolutely. Please select from the following stores: \n " + str(string))

        user_data[str(update.message["chat"]["id"])]["initial_stores"] = stores

        # user_data[str(update.message["chat"]["id"])]["last"] = "eatchoice"
    elif selection == "Cancel Order":
        update.message.reply_text("Okay. I'll cancel your order. What was your order ID? (i.e. 12345)")

    elif selection == "Check Delivery Status":
        update.message.reply_text("Let's check on that order for you. What was your order ID? (i.e. 12345)")

    elif selection == "Promotions":
        update.message.reply_text("Here are the exclusive promotions we offer:")
        r = requests.get("https://limitless-harbor-23385.herokuapp.com/promotions")
        json = r.json()
        promotions = json["data"]
        output_string = ""
        for promotion in promotions:
            curr_promo = promotions[promotion]["name"] + "\nID: " + promotions[promotion]["id"]+ "\n" + promotions[promotion]["description"] + "\nPromo Price: " + promotions[promotion]["price"] + "\n\n"
            output_string = output_string + curr_promo
        reply_keyboard = [['Done']]
        markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
        update.message.reply_text(output_string)
        update.message.reply_text("The promotions are accessible through the Order Food tab, and are reflected in the menu listings.", reply_markup=markup)

        user_data.clear()
        return ConversationHandler.END
    elif selection == "Fufill Order":
        r = requests.get("https://limitless-harbor-23385.herokuapp.com/orders")
        json = r.json()
        orders = json["data"]["orders"]
        orders.reverse()
        master_string = "Here is what you can fufill: "
        for order in orders:
            order_string = ""
            for key in order:
                if key == "order_id":
                    order_string = order_string + "\n" + "Order ID" + ": " + str(order[key])
                if key == "items":
                    order_string = order_string + "\n" + "Items:"
                    for item in order["items"]:
                        order_string = order_string + "\n\t" + str(order["items"][item]["store"]) + " - " + str(order["items"][item]["item"]) + " x" + str(order["items"][item]["quantity"])
                if key == "commision":
                    order_string = order_string + "\n" + "Commission" + ": " + str(order[key])
                if key == "customer_name":
                    order_string = order_string + "\n" + "Customer Name" + ": " + str(order[key])

            master_string = master_string + "\n" + order_string
        update.message.reply_text(master_string)
        update.message.reply_text("\nEnter the order ID of the order you would like to fulfill, or 0 to exit.")

    elif selection == "Help":
        if text == "Go Back":
            user_data.clear()
            return ConverstaionHandler.END

        reply_keyboard = [["Go Back", "Support"]]
        markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
        update.message.reply_text("Hi welcome to the crowdpotato chatbot. To place an order please click the place an order button and fill out the necessary details. We will match you with a courier soon! to see what orders are available currently to fulfill, click the place an order button. To view special deals be sure to check out the promotions on offer! https://www.crowdpotato.co/",reply_markup=markup)

        # user_data.clear()
        # return ConversationHandler.END
    # update.message.reply_text(
        # 'I can definitely help you{}? Yes, I would love to hear about that!'.format(text.lower()))
    else:
        update.message.reply_text("Feature coming soon.")
        user_data.clear()
        return ConversationHandler.END
    return TYPING_REPLY


def custom_choice(bot, update):
    update.message.reply_text('Alright, please send me the category first, '
                              'for example "Most impressive skill"')

    return TYPING_CHOICE


def received_information(bot, update, user_data):
    text = update.message.text
    category = user_data[str(update.message["chat"]["id"])]['topic']

    # user_data[category] = text
    print("Checking dialogue...")
    print("Current Dialogue " + category)
    print("User response" + text)
    print("Dict " + str(user_data[str(update.message["chat"]["id"])]))
    # del user_data['topic']
    print(category)
    if category == "Order Food":
        if "food_type" not in user_data[str(update.message["chat"]["id"])]:# and user_data[str(update.message["chat"]["id"])]["last"] == "eatchoice":
            #just picked food
                # Food Store Variables
            user_data[str(update.message["chat"]["id"])]["stores_done"] = False
            user_data[str(update.message["chat"]["id"])]["QUANTITY_FLAG"] = False
            user_data[str(update.message["chat"]["id"])]["food_choice_done"] = False
            user_data[str(update.message["chat"]["id"])]["curr_store"] = "0"
            user_data[str(update.message["chat"]["id"])]["cart_price"] = 0
            user_data[str(update.message["chat"]["id"])]["food_order_complete"] = False
            store_id = text
            chosen_store = "N/A"
            initial_stores = user_data[str(update.message["chat"]["id"])]["initial_stores"]
            for initial_store in initial_stores:
                if str(initial_store["store_id"]) == str(store_id):
                    chosen_store = initial_store["store_name"]
            if chosen_store == "N/A":
                #If not a valid choice ask to try again
                update.message.reply_text("Hmmm... it seems this store isn't on our list. Try again.")
            else:
                user_data[str(update.message["chat"]["id"])]["curr_store"] = store_id
                user_data[str(update.message["chat"]["id"])]["food_type"] = {}
                user_data[str(update.message["chat"]["id"])]["order"] = "" + chosen_store + ":\n"
                update.message.reply_text("Awesome, " + chosen_store + " sounds super good right now. Here is what have to they offer, select from the following:")
                r = requests.get("https://limitless-harbor-23385.herokuapp.com/stores?store_id=" + str(store_id))
                json = r.json()
                store_data = json["data"]
                store_choices = ""
                options = store_data["store_items"]

                t = 0
                for option in options:
                    if t==0:
                        t=5
                        continue

                    store_choices = store_choices + str(option["id"]) + ".\t" + str(option["name"]) + " $" + str(option["price"]) + "\n"

                update.message.reply_text(str(store_choices))
                user_data[str(update.message["chat"]["id"])]["initial_menu_items"] = options

        elif "food_type" in user_data[str(update.message["chat"]["id"])] and (user_data[str(update.message["chat"]["id"])]["curr_store"] + "-" + text) not in user_data[str(update.message["chat"]["id"])]["food_type"] and user_data[str(update.message["chat"]["id"])]["food_order_complete"] == False and text != "0" and user_data[str(update.message["chat"]["id"])]["food_choice_done"] == False and user_data[str(update.message["chat"]["id"])]["QUANTITY_FLAG"] == False:
            food_choice = text
            user_data[str(update.message["chat"]["id"])]["food_choice"] = text
            selected_food = "N/A"
            curr_selection = {}
            curr_price = "0"
            initial_menu_items = user_data[str(update.message["chat"]["id"])]["initial_menu_items"]
            t = 0

            for initial_menu in initial_menu_items:
                if t == 0:
                    t = 5
                    continue

                if str(initial_menu["id"]) == str(food_choice):
                    #user_data[str(update.message["chat"]["id"])]["cart_price"] += float(initial_menu["price"])
                    curr_selection = initial_menu
                    curr_price = initial_menu["price"]
                    user_data[str(update.message["chat"]["id"])]["curr_price"] = initial_menu["price"]
                    selected_food = initial_menu["name"]
                    user_data[str(update.message["chat"]["id"])]["selected_food"] = initial_menu["name"]

            if selected_food == "N/A":
                update.message.reply_text("Hmmm... it seems this isn't on the menu. Try again.")
            else:
                update.message.reply_text("Cool, I'm adding " + selected_food + " to your cart.")
                user_data[str(update.message["chat"]["id"])]["QUANTITY_FLAG"] = True
                user_data[str(update.message["chat"]["id"])]["concat_string"] = str(user_data[str(update.message["chat"]["id"])]["curr_store"]) + "-" + text
                update.message.reply_text("How many do you want to add to your cart?")

        elif "concat_string" in user_data[str(update.message["chat"]["id"])] and user_data[str(update.message["chat"]["id"])]["QUANTITY_FLAG"] == True and "add_flag" not in user_data[str(update.message["chat"]["id"])]:
                if int(text) > 0:
                    initial_stores = user_data[str(update.message["chat"]["id"])]["initial_stores"]

                    store_english = ""
                    item_stuff = {}

                    for store in initial_stores:
                        if str(store["store_id"]) == str(user_data[str(update.message["chat"]["id"])]["curr_store"]):
                            item_stuff[user_data[str(update.message["chat"]["id"])]["concat_string"]] = {"store":store["store_name"], "item":user_data[str(update.message["chat"]["id"])]["selected_food"], "price": user_data[str(update.message["chat"]["id"])]["curr_price"], "quantity": str(text), "store_id":store["store_id"], "item_id":str(user_data[str(update.message["chat"]["id"])]["food_choice"])}

                    user_data[str(update.message["chat"]["id"])]["cart_price"] += float(float(item_stuff[user_data[str(update.message["chat"]["id"])]["concat_string"]]["price"]) * int(text))
                    user_data[str(update.message["chat"]["id"])]["food_type"][user_data[str(update.message["chat"]["id"])]["concat_string"]] = item_stuff[user_data[str(update.message["chat"]["id"])]["concat_string"]]
                    user_data[str(update.message["chat"]["id"])]["order"] += text + " x " + user_data[str(update.message["chat"]["id"])]["selected_food"] + " - $" + item_stuff[user_data[str(update.message["chat"]["id"])]["concat_string"]]["price"] + "\n"
                    user_data[str(update.message["chat"]["id"])]["QUANTITY_FLAG"] = False
                    update.message.reply_text("Want to add anything else? See the options above, or enter 0 to finish.")
                else:
                    update.message.reply_text("Please enter an amount greater than 0.")

        elif "food_type" in user_data[str(update.message["chat"]["id"])] and user_data[str(update.message["chat"]["id"])]["food_choice_done"] == False and "add_flag" not in user_data[str(update.message["chat"]["id"])]:
            next_choice = text
            selected_food = "N/A"
            if next_choice == str(0):
                user_data[str(update.message["chat"]["id"])]["food_choice_done"] = True
                update.message.reply_text("You're all set! Do you want anything from anywhere else?")
                r = requests.get("https://limitless-harbor-23385.herokuapp.com/stores")
                json = r.json()
                stores = json["data"]
                string = ""

                for store in stores:
                    string = string + "\n " + str(store["store_id"]) + ".\t" + str(store["store_name"])


                update.message.reply_text("Here is the list again: \n " + str(string) + " \n\nDone Ordering, 0")

            else:
                initial_menu_items = user_data[str(update.message["chat"]["id"])]["initial_menu_items"]
                t = 0

                for initial_menu in initial_menu_items:
                    if t == 0:
                        t = 5
                        continue

                    if str(initial_menu["id"]) == str(next_choice):
                        #user_data[str(update.message["chat"]["id"])]["cart_price"] += float(initial_menu["price"])
                        selected_food = initial_menu["name"]

                initial_stores = user_data[str(update.message["chat"]["id"])]["initial_stores"]

                store_english = ""

                for store in initial_stores:
                    if store["store_id"] == str(user_data[str(update.message["chat"]["id"])]["curr_store"]):
                        store_english = store["store_name"]

                concat_string = str(user_data[str(update.message["chat"]["id"])]["curr_store"]) + "-" + text
                if concat_string in user_data[str(update.message["chat"]["id"])]:
                    #update quantity
                    user_data[str(update.message["chat"]["id"])][concat_string]["quantity"] = str(int(user_data[str(update.message["chat"]["id"])]["food_type"][concat_string]["quantity"]) + 1)
                else:
                    user_data[str(update.message["chat"]["id"])][concat_string] = {"store":store_english, "item":selected_food, "price": initial_menu["price"], "quantity": "1", "store_id":store["store_id"], "item_id":str(food_choice)}

                if selected_food == "N/A":
                    update.message.reply_text("Hmmm... it seems this isn't on our list. Try again.")
                else:
                    user_data[str(update.message["chat"]["id"])]["order"] += "\t" + selected_food + "\n"
                    update.message.reply_text("I've added " + selected_food + " to your cart. How many should I add?")
                    #update.message.reply_text("Want to add anything else? See the options above, or enter 0 to finish.")

        elif user_data[str(update.message["chat"]["id"])]["food_choice_done"] == True and user_data[str(update.message["chat"]["id"])]["stores_done"] == False and "add_flag" not in user_data[str(update.message["chat"]["id"])]:
            store_id = text
            chosen_store = "N/A"
            initial_stores = user_data[str(update.message["chat"]["id"])]["initial_stores"]
            for initial_store in initial_stores:
                if str(initial_store["store_id"]) == str(store_id):
                    chosen_store = initial_store["store_name"]

            if store_id == str(0):
                user_data[str(update.message["chat"]["id"])]["stores_done"] = True
                user_data[str(update.message["chat"]["id"])]["food_choice_done"] = True
                user_data[str(update.message["chat"]["id"])]["food_order_complete"] = True
                update.message.reply_text("Great. Where can we deliver this to?")
            else:
                if chosen_store == "N/A":
                    #If not a valid choice ask to try again
                    update.message.reply_text("Hmmm... it seems this isn't on our list. Try again.")
                else:
                    user_data[str(update.message["chat"]["id"])]["food_choice_done"] = False
                    user_data[str(update.message["chat"]["id"])]["curr_store"] = store_id
                    user_data[str(update.message["chat"]["id"])]["order"] += "" + chosen_store + ":\n"
                    update.message.reply_text("Awesome, " + chosen_store + " sounds super good right now. Here is what have to they offer, select from the following:")
                    r = requests.get("https://limitless-harbor-23385.herokuapp.com/stores?store_id=" + str(store_id))
                    json = r.json()
                    store_data = json["data"]
                    store_choices = ""
                    options = store_data["store_items"]

                    t = 0
                    for option in options:
                        if t==0:
                            t=5
                            continue

                        store_choices = store_choices + str(option["id"]) + ".\t" + str(option["name"]) + " $" + str(option["price"]) + "\n"

                    update.message.reply_text(str(store_choices))
                    user_data[str(update.message["chat"]["id"])]["initial_menu_items"] = options

        elif user_data[str(update.message["chat"]["id"])]["food_choice_done"] == True and user_data[str(update.message["chat"]["id"])]["stores_done"] == True and "address" not in user_data[str(update.message["chat"]["id"])]:
            update.message.reply_text("Awesome, I'll have food delivered to " + text + ". \nNow tell me, what is a good contact phone number for you? (Number should be 8 digits beginning with 6, 8, or 9)")
            user_data[str(update.message["chat"]["id"])]["address"] = text

        elif "address" in user_data[str(update.message["chat"]["id"])] and "contact" not in user_data[str(update.message["chat"]["id"])]:
            text = text.replace(" ", "")
            if len(text) != 8 or (text[0] == '0' or text[0] == '1' or text[0] == '2' or text[0] == '3' or text[0] == '4' or text[0] == '5' or text[0] == '7'):
                update.message.reply_text("Invalid number format please try again.")
            else:
                user_data[str(update.message["chat"]["id"])]["contact"] = text
                update.message.reply_text("Okay, I noted that your contact number is" + text  + ". What commission would you like to give the delivery man?")

        elif "contact" in user_data[str(update.message["chat"]["id"])] and "commission" not in user_data[str(update.message["chat"]["id"])]:
            if int(text) >= 0:
                user_data[str(update.message["chat"]["id"])]["commission"] = text
                user_data[str(update.message["chat"]["id"])]["cart_price"] = float(user_data[str(update.message["chat"]["id"])]["cart_price"]) + float(text)
                reply_keyboard = [['Paylah/Paynow', 'Card']]
                markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
                update.message.reply_text("Okay, the courier will receive " + text  + " as a commission. Are you paying with cash or card?", reply_markup=markup)
            else:
                update.message.reply_text("Please enter a valid commission, 0 or greater.")

        elif "commission" in user_data[str(update.message["chat"]["id"])] and "payment_method" not in user_data[str(update.message["chat"]["id"])]:
            user_data[str(update.message["chat"]["id"])]["cart_price"] = float("{0:.2f}".format(user_data[str(update.message["chat"]["id"])]["cart_price"]))
            if text == "Paylah/Paynow":
                user_data[str(update.message["chat"]["id"])]["payment_method"] = text
                reply_keyboard = [['Confirm', 'Edit', 'Cancel']]
                markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)

                update.message.reply_text("Okay please confirm your order:\n\n" + user_data[str(update.message["chat"]["id"])]["order"] + "\nAddress: " + str(user_data[str(update.message["chat"]["id"])]["address"]) + "\nContact Number: " + str(user_data[str(update.message["chat"]["id"])]["contact"]) + "\nPayment Info: " + str(user_data[str(update.message["chat"]["id"])]["payment_method"]) + "\n\nTotal Price: $" + str(user_data[str(update.message["chat"]["id"])]["cart_price"]), reply_markup=markup)
            elif text == "Card":
                user_data[str(update.message["chat"]["id"])]["payment_method"] = text
                reply_keyboard = [['Confirm', 'Edit', 'Cancel']]
                markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)

                update.message.reply_text("Okay please confirm your order:\n\n" + user_data[str(update.message["chat"]["id"])]["order"] + "\nAddress: " + str(user_data[str(update.message["chat"]["id"])]["address"]) + "\nContact Number: " + str(user_data[str(update.message["chat"]["id"])]["contact"]) + "\nPayment Info: " + str(user_data[str(update.message["chat"]["id"])]["payment_method"]) + "\n\nTotal Price: $" + str(user_data[str(update.message["chat"]["id"])]["cart_price"]), reply_markup=markup)

        elif "payment_method" in user_data[str(update.message["chat"]["id"])] and "confirmation" not in user_data[str(update.message["chat"]["id"])]:# and user_data[str(update.message["chat"]["id"])]["payment_method"] == "Cash":
            user_data[str(update.message["chat"]["id"])]["confirmation"] = text
            #coming from confirm order please
            if text == "Confirm":
                # api-endpoint

                update.message.reply_text("Give me a moment to submit your order.")
                URL = "https://limitless-harbor-23385.herokuapp.com/orders"

                # defining a params dict for the parameters to be sent to the API

                PARAMS = {'customer_name':str(update.message["chat"]["username"]), "customer_address":str(user_data[str(update.message["chat"]["id"])]["address"]), "commision":str(user_data[str(update.message["chat"]["id"])]["commission"]), "payment_details":str(user_data[str(update.message["chat"]["id"])]["payment_method"]),
                "customer_contact":str(user_data[str(update.message["chat"]["id"])]["contact"])}

                # sending post request and saving the response as response object
                r = requests.post(url = URL, data = PARAMS)
                # extracting data in json format
                data = r.json()


                for ea in user_data[str(update.message["chat"]["id"])]["food_type"]:
                    p = requests.put(url = URL, data = {"order_id":str(data["data"]["order_id"]), "new_item_store_id": str(user_data[str(update.message["chat"]["id"])]["food_type"][ea]["store_id"]), "new_item_id":str(user_data[str(update.message["chat"]["id"])]["food_type"][ea]["item_id"]), "new_item_quantity":str(user_data[str(update.message["chat"]["id"])]["food_type"][ea]["quantity"]), "cost": "$" + str(user_data[str(update.message["chat"]["id"])]["cart_price"])})


                #p = requests.put(url = URL + "?order_id=" + str(data["data"]["order_id"]), params = {"items":user_data[str(update.message["chat"]["id"])]["food_type"]})
                update.message.reply_text("Your order has been sent with ID " + str(data["data"]["order_id"]))
                update.message.reply_text("Please pay for your order using the following link. You will not be charged until your order is delivered.")
                update.message.reply_text("https://nameless-coast-87321.herokuapp.com/#/login")
                update.message.reply_text("You can track your order using your order ID from the Check Order Status section.")
                user_data.clear()
                return ConversationHandler.END


            elif text == "Edit":
                user_data[str(update.message["chat"]["id"])]["EDIT_PHASE"] = 0
                user_data[str(update.message["chat"]["id"])]["add_flag"] = False
                user_data[str(update.message["chat"]["id"])]["EDIT_FLAG"] = True
                out = "Here is what is in your cart, select the ID of what you'd like to edit (-1 to finish) or add a new item:"
                x = 0
                user_data[str(update.message["chat"]["id"])]["arr"] = []
                for ea in user_data[str(update.message["chat"]["id"])]["food_type"]:
                    out = out + "\n" + str(x) + ".\t" + str(user_data[str(update.message["chat"]["id"])]["food_type"][ea]["item"]) + " (" + str(user_data[str(update.message["chat"]["id"])]["food_type"][ea]["store"]) + ")"
                    user_data[str(update.message["chat"]["id"])]["arr"].append(ea)
                    x += 1
                reply_keyboard = [['Add New Item']]
                markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
                update.message.reply_text(out, reply_markup=markup)
            elif text == "Cancel":
                update.message.reply_text("Order cancelled.")
                user_data.clear()
                return ConversationHandler.END


        elif "confirmation" in user_data[str(update.message["chat"]["id"])] and user_data[str(update.message["chat"]["id"])]["EDIT_FLAG"] == True and user_data[str(update.message["chat"]["id"])]["add_flag"] == False and user_data[str(update.message["chat"]["id"])]["EDIT_PHASE"] == 0:
            choice = text
            if choice == str(-1):
                user_data[str(update.message["chat"]["id"])]["EDIT_FLAG"] = False
                user_data[str(update.message["chat"]["id"])].pop("confirmation")
                reply_keyboard = [['Confirm', 'Edit', 'Cancel']]
                markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
                update.message.reply_text("Okay, here is your cart. Please confirm your order:\n\n" + user_data[str(update.message["chat"]["id"])]["order"] + "\nAddress: " + str(user_data[str(update.message["chat"]["id"])]["address"]) + "\nContact Number: " + str(user_data[str(update.message["chat"]["id"])]["contact"]) + "\nPayment Info: " + str(user_data[str(update.message["chat"]["id"])]["payment_method"]) + "\n\nTotal Price: $" + str(user_data[str(update.message["chat"]["id"])]["cart_price"]), reply_markup=markup)

            elif choice == "Add New Item":
                user_data[str(update.message["chat"]["id"])]["add_flag"] = True
                user_data[str(update.message["chat"]["id"])]["EDIT_PHASE"] = 1
                update.message.reply_text("Select where you want to order from:")
                r = requests.get("https://limitless-harbor-23385.herokuapp.com/stores")
                json = r.json()
                stores = json["data"]
                string = ""

                for store in stores:
                    string = string + "\n " + str(store["store_id"]) + ".\t" + str(store["store_name"])

                update.message.reply_text("Here is the list again: \n " + str(string) + " \n\nDone Ordering, 0")
            else:
                user_data[str(update.message["chat"]["id"])]["EDIT_PHASE"] = 2
                out = "You've selected " + str(user_data[str(update.message["chat"]["id"])]["food_type"][user_data[str(update.message["chat"]["id"])]["arr"][int(choice)]]["item"]) + ". Enter the new quantity you want (0 to remove)."
                update.message.reply_text(out)
                user_data[str(update.message["chat"]["id"])]["EDIT_KEY"] = user_data[str(update.message["chat"]["id"])]["arr"][int(choice)]

        elif "confirmation" in user_data[str(update.message["chat"]["id"])] and user_data[str(update.message["chat"]["id"])]["add_flag"] == False and user_data[str(update.message["chat"]["id"])]["EDIT_PHASE"] == 2:
            q = text
            removed = {}
            if q == str(0):
                removed = user_data[str(update.message["chat"]["id"])]["food_type"].pop(user_data[str(update.message["chat"]["id"])]["EDIT_KEY"])
                user_data[str(update.message["chat"]["id"])]["cart_price"] = float(user_data[str(update.message["chat"]["id"])]["cart_price"]) - float(float(removed["price"]) * int(removed["quantity"]))
                user_data[str(update.message["chat"]["id"])]["order"] = ""

                for ea in user_data[str(update.message["chat"]["id"])]["food_type"]:
                    user_data[str(update.message["chat"]["id"])]["order"] = user_data[str(update.message["chat"]["id"])]["order"] + str(user_data[str(update.message["chat"]["id"])]["food_type"][ea]["store"]) + ":\n\t" + str(user_data[str(update.message["chat"]["id"])]["food_type"][ea]["item"]) + "\n"

                user_data[str(update.message["chat"]["id"])]["order"] = user_data[str(update.message["chat"]["id"])]["order"] + "\nAddress: " + str(user_data[str(update.message["chat"]["id"])]["address"]) + "\nContact Number: " + str(user_data[str(update.message["chat"]["id"])]["contact"]) + "\nPayment Info: " + str(user_data[str(update.message["chat"]["id"])]["payment_method"])
            else:
                user_data[str(update.message["chat"]["id"])]["cart_price"] = float(user_data[str(update.message["chat"]["id"])]["cart_price"]) + float(float(q) * float(user_data[str(update.message["chat"]["id"])]["food_type"][user_data[str(update.message["chat"]["id"])]["EDIT_KEY"]]["price"])) - float(float(user_data[str(update.message["chat"]["id"])]["food_type"][user_data[str(update.message["chat"]["id"])]["EDIT_KEY"]]["quantity"]) * float(user_data[str(update.message["chat"]["id"])]["food_type"][user_data[str(update.message["chat"]["id"])]["EDIT_KEY"]]["price"]))
                user_data[str(update.message["chat"]["id"])]["food_type"][user_data[str(update.message["chat"]["id"])]["EDIT_KEY"]]["quantity"] = q

            user_data[str(update.message["chat"]["id"])]["EDIT_FLAG"] = False
            user_data[str(update.message["chat"]["id"])].pop("confirmation")
            reply_keyboard = [['Confirm', 'Edit', 'Cancel']]
            markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)

            update.message.reply_text("Okay, I've updated your cart. Please confirm your order:\n\n" + user_data[str(update.message["chat"]["id"])]["order"] + "\nAddress: " + str(user_data[str(update.message["chat"]["id"])]["address"]) + "\nContact Number: " + str(user_data[str(update.message["chat"]["id"])]["contact"]) + "\nPayment Info: " + str(user_data[str(update.message["chat"]["id"])]["payment_method"]) + "\n\nTotal Price: $" + str(user_data[str(update.message["chat"]["id"])]["cart_price"]), reply_markup=markup)

        elif "confirmation" in user_data[str(update.message["chat"]["id"])] and user_data[str(update.message["chat"]["id"])]["add_flag"] == True and user_data[str(update.message["chat"]["id"])]["EDIT_PHASE"] == 1:
            store_id = text

            if store_id == str(0):
                user_data[str(update.message["chat"]["id"])]["EDIT_FLAG"] = False
                user_data[str(update.message["chat"]["id"])].pop("confirmation")
                reply_keyboard = [['Confirm', 'Edit', 'Cancel']]
                markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
                update.message.reply_text("Okay, here is your cart. Please confirm your order:\n\n" + user_data[str(update.message["chat"]["id"])]["order"] + "\nAddress: " + str(user_data[str(update.message["chat"]["id"])]["address"]) + "\nContact Number: " + str(user_data[str(update.message["chat"]["id"])]["contact"]) + "\nPayment Info: " + str(user_data[str(update.message["chat"]["id"])]["payment_method"]) + "\n\nTotal Price: $" + str(user_data[str(update.message["chat"]["id"])]["cart_price"]), reply_markup=markup)
            else:
                chosen_store = "N/A"
                initial_stores = user_data[str(update.message["chat"]["id"])]["initial_stores"]
                for initial_store in initial_stores:
                    if str(initial_store["store_id"]) == str(store_id):
                        chosen_store = initial_store["store_name"]

                if chosen_store == "N/A":
                    update.message.reply_text("Hmmm... it seems this isn't on our list. Try again.")
                else:
                    update.message.reply_text("Awesome, " + chosen_store + " sounds super good right now. Here is what have to they offer, select from the following:")
                    user_data[str(update.message["chat"]["id"])]["curr_store"] = store_id

                    r = requests.get("https://limitless-harbor-23385.herokuapp.com/stores?store_id=" + str(store_id))
                    json = r.json()
                    store_data = json["data"]
                    store_choices = ""
                    options = store_data["store_items"]

                    wait = 0
                    for option in options:
                        if wait == 0:
                            wait = 1
                            continue

                        store_choices = store_choices + str(option["id"]) + ".\t" + str(option["name"]) + " $" + str(option["price"]) + "\n"

                    update.message.reply_text(str(store_choices))
                    user_data[str(update.message["chat"]["id"])]["initial_menu_items"] = options
                    user_data[str(update.message["chat"]["id"])]["EDIT_PHASE"] = 3
                    user_data[str(update.message["chat"]["id"])]["order"] += chosen_store + ":\n"

        elif "confirmation" in user_data[str(update.message["chat"]["id"])] and user_data[str(update.message["chat"]["id"])]["add_flag"] == True and user_data[str(update.message["chat"]["id"])]["EDIT_PHASE"] == 3:
            next_choice = text
            selected_food = "N/A"
            if next_choice == str(0):
                user_data[str(update.message["chat"]["id"])]["EDIT_FLAG"] = False
                user_data[str(update.message["chat"]["id"])].pop("confirmation")
                reply_keyboard = [['Confirm', 'Edit', 'Cancel']]
                markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
                update.message.reply_text("Okay, here is your cart. Please confirm your order:\n\n" + user_data[str(update.message["chat"]["id"])]["order"] + "\nAddress: " + str(user_data[str(update.message["chat"]["id"])]["address"]) + "\nContact Number: " + str(user_data[str(update.message["chat"]["id"])]["contact"]) + "\nPayment Info: " + str(user_data[str(update.message["chat"]["id"])]["payment_method"]) + "\n\nTotal Price: $" + str(user_data[str(update.message["chat"]["id"])]["cart_price"]), reply_markup=markup)

            else:
                initial_menu_items = user_data[str(update.message["chat"]["id"])]["initial_menu_items"]

                wait = 0
                for initial_menu in initial_menu_items:
                    if wait == 0:
                        wait = 1
                        continue

                    if str(initial_menu["id"]) == str(next_choice):
                        selected_food = initial_menu["name"]

                initial_stores = user_data[str(update.message["chat"]["id"])]["initial_stores"]

                #store_english = ""
                #
                # for store in initial_stores:
                #     if store["store_id"] == str(user_data[str(update.message["chat"]["id"])]["curr_store"]):
                #         store_english = store[""]

                user_data[str(update.message["chat"]["id"])]["concat_string"] = str(user_data[str(update.message["chat"]["id"])]["curr_store"]) + "-" + text

                if selected_food == "N/A":
                    update.message.reply_text("Hmmm... it seems this isn't on our list. Try again.")
                else:
                    user_data[str(update.message["chat"]["id"])]["selected_food"] = selected_food
                    user_data[str(update.message["chat"]["id"])]["EDIT_PHASE"] = 4
                    update.message.reply_text("I've added " + selected_food + " to your cart. How many should I add?")

        elif "confirmation" in user_data[str(update.message["chat"]["id"])] and user_data[str(update.message["chat"]["id"])]["add_flag"] == True and user_data[str(update.message["chat"]["id"])]["EDIT_PHASE"] == 4:
            if text == str(0):
                user_data[str(update.message["chat"]["id"])].pop("confirmation")
                reply_keyboard = [['Confirm', 'Edit', 'Cancel']]
                markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)

                user_data[str(update.message["chat"]["id"])]["add_flag"] = False

                update.message.reply_text("Okay, I've added nothing. Please confirm your order:\n\n" + user_data[str(update.message["chat"]["id"])]["order"] + "\nAddress: " + str(user_data[str(update.message["chat"]["id"])]["address"]) + "\nContact Number: " + str(user_data[str(update.message["chat"]["id"])]["contact"]) + "\nPayment Info: " + str(user_data[str(update.message["chat"]["id"])]["payment_method"]) + "\n\nTotal Price: $" + str(user_data[str(update.message["chat"]["id"])]["cart_price"]), reply_markup=markup)
            else:
                initial_stores = user_data[str(update.message["chat"]["id"])]["initial_stores"]

                store_english = ""
                item_stuff = {}

                for store in initial_stores:
                    if str(store["store_id"]) == str(user_data[str(update.message["chat"]["id"])]["curr_store"]):
                        item_stuff[user_data[str(update.message["chat"]["id"])]["concat_string"]] = {"store":store["store_name"], "item":user_data[str(update.message["chat"]["id"])]["selected_food"], "price": user_data[str(update.message["chat"]["id"])]["curr_price"], "quantity": str(text), "store_id":store["store_id"], "item_id":str(user_data[str(update.message["chat"]["id"])]["food_choice"])}

                user_data[str(update.message["chat"]["id"])]["cart_price"] += float(float(item_stuff[user_data[str(update.message["chat"]["id"])]["concat_string"]]["price"]) * int(text))
                user_data[str(update.message["chat"]["id"])]["food_type"][user_data[str(update.message["chat"]["id"])]["concat_string"]] = item_stuff[user_data[str(update.message["chat"]["id"])]["concat_string"]]
                user_data[str(update.message["chat"]["id"])]["order"] += "\t" + user_data[str(update.message["chat"]["id"])]["selected_food"] + "\n"
                user_data[str(update.message["chat"]["id"])]["EDIT_PHASE"] = 3
                update.message.reply_text("Want to add anything else? See the options above, or enter 0 to finish.")

    elif category == "Check Delivery Status" and "order_status" not in user_data[str(update.message["chat"]["id"])]:
        user_data[str(update.message["chat"]["id"])]["order_status"] = text
        update.message.reply_text("Awesome, I will look into the order number: " + user_data[str(update.message["chat"]["id"])]["order_status"] + ". Give me a moment...")
        r = requests.get("https://limitless-harbor-23385.herokuapp.com/orders?order_id=" + str(text))
        try:
            json = r.json()
            status = json["data"]["order_data"]["order_status"]
            reply_keyboard = [['Done']]
            markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
            update.message.reply_text("Your order is currently listed as: " + status, reply_markup=markup)
            user_data.clear()
            return ConversationHandler.END
        except:
            reply_keyboard = [['Done']]
            markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
            update.message.reply_text("Sorry, this is not a valid order.", reply_markup=markup)
            user_data.clear()
            return ConversationHandler.END


    elif category == "Cancel Order" and "cancel" not in user_data[str(update.message["chat"]["id"])]:
        user_data[str(update.message["chat"]["id"])]["cancel"] = text
        order_url = "https://limitless-harbor-23385.herokuapp.com/orders"
        p = requests.put(url = order_url, data = {"order_id":user_data[str(update.message["chat"]["id"])]["cancel"], "order_status": "CANCELLED"})
        reply_keyboard = [['Done']]
        markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
        update.message.reply_text("Your order has been cancelled.", reply_markup=markup)
        user_data.clear()
        return ConversationHandler.END

    elif category == "Fufill Order":

        if "fulfill_order_id" not in user_data[str(update.message["chat"]["id"])]:
            if text == str(0):
                update.message.reply_text("Okay, you're not going to fulfill anything.")
                user_data.clear()
                return ConversationHandler.END
            r = requests.get("https://limitless-harbor-23385.herokuapp.com/orders?order_id=" + str(text))
            json = r.json()
            if json["data"]["order_data"] is None:
                update.message.reply_text("Sorry, this is not a valid order number. Try again")
            else:
                user_data[str(update.message["chat"]["id"])]["fulfill_order_id"] = text
                user_data[str(update.message["chat"]["id"])]["courier_customer_name"] = json["data"]["order_data"]["customer_name"]
                user_data[str(update.message["chat"]["id"])]["customer_contact"] = json["data"]["order_data"]["customer_contact"]
                update.message.reply_text("Great. You have selected order #" + text)
                print_string = ""
                for item in json["data"]["order_data"]["items"]:
                    print_string = print_string + "\n\t\t" + str(json["data"]["order_data"]["items"][item]["store"]) + " - " + str(json["data"]["order_data"]["items"][item]["item"]) + " x" + str(json["data"]["order_data"]["items"][item]["quantity"])
                    print_string = print_string + "\n"
                update.message.reply_text("ORDER INFO:\n\t" + "Customer Username: " + json["data"]["order_data"]["customer_name"] + "\n\tCustomer Address: " + json["data"]["order_data"]["customer_address"] + "\n\tOrder Total: " + str(json["data"]["order_data"]["cost"]) + "\n\tItems: " + print_string)

                update.message.reply_text("Please enter a phone # you can be reached at: (Should be 8 digits beginning with 6, 8, or 9)")

        elif "fulfill_order_id" in user_data[str(update.message["chat"]["id"])] and "courier_contact" not in user_data[str(update.message["chat"]["id"])]:
            text = text.replace(" ", "")
            if len(text) != 8 or (text[0] == '0' or text[0] == '1' or text[0] == '2' or text[0] == '3' or text[0] == '4' or text[0] == '5' or text[0] == '7'):
                update.message.reply_text("Invalid number format please try again.")
                flag = False
            else:
                user_data[str(update.message["chat"]["id"])]["courier_contact"] = text
                flag = True

            if flag:
                update.message.reply_text("Great. Your number is: " + text)
                user_data[str(update.message["chat"]["id"])]["courier_name"] = str(update.message["chat"]["first_name"])
                order_url = "https://limitless-harbor-23385.herokuapp.com/orders"
                p = requests.put(url = order_url, data = {"order_id":user_data[str(update.message["chat"]["id"])]["fulfill_order_id"],"courier_contact":str(user_data[str(update.message["chat"]["id"])]["courier_contact"]), "courier_name":str(user_data[str(update.message["chat"]["id"])]["courier_name"]), "order_status": "matched"})
                update.message.reply_text("You're now assigned to this order!")
                update.message.reply_text("You can message the customer: @" + str(user_data[str(update.message["chat"]["id"])]["courier_customer_name"]))
                reply_keyboard = [['Picking Up']]
                markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
                update.message.reply_text("Let us know when you are picking up the order.", reply_markup=markup)

        elif "courier_name" in user_data[str(update.message["chat"]["id"])] and "fulfill_status" not in user_data[str(update.message["chat"]["id"])]:
            order_status = text
            order_url = "https://limitless-harbor-23385.herokuapp.com/orders"
            p = requests.put(url = order_url, data = {"order_id":user_data[str(update.message["chat"]["id"])]["fulfill_order_id"], "order_status": "Picking Up"})
            user_data[str(update.message["chat"]["id"])]["fulfill_status"] = "Picking Up"
            account_sid = 'AC786497ab3dd58ca05c5ad0b2096e9755'
            auth_token = '1f64b515c4f25aa8642fa617b7f7bd40'
            client = Client(account_sid, auth_token)
            message = client.messages.create(
                from_='+14092455639',
                body = 'Hello, CrowdPotato here! Your order is currently being picked up from the store!',
                to = str(user_data[str(update.message["chat"]["id"])]["customer_contact"])
            )
            print(message.sid)
            update.message.reply_text("What was the total price of the order?")

        elif "fulfill_status" in user_data[str(update.message["chat"]["id"])] and user_data[str(update.message["chat"]["id"])]["fulfill_status"] == "Picking Up":
            order_total = text
            order_url = "https://limitless-harbor-23385.herokuapp.com/orders"
            p = requests.put(url = order_url, data = {"order_id":user_data[str(update.message["chat"]["id"])]["fulfill_order_id"], "cost": float(text)})
            reply_keyboard = [['En Route']]
            user_data[str(update.message["chat"]["id"])]["fulfill_status"] = "Paid"
            markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
            update.message.reply_text("Got it. Please let us know when you are en route to the customer.", reply_markup=markup)

        elif text == "En Route" and user_data[str(update.message["chat"]["id"])]["fulfill_status"] == "Paid":
            order_url = "https://limitless-harbor-23385.herokuapp.com/orders"
            p = requests.put(url = order_url, data = {"order_id":user_data[str(update.message["chat"]["id"])]["fulfill_order_id"], "order_status": "En Route"})
            user_data[str(update.message["chat"]["id"])]["fulfill_status"] = "En Route"
            account_sid = 'AC786497ab3dd58ca05c5ad0b2096e9755'
            auth_token = '1f64b515c4f25aa8642fa617b7f7bd40'
            client = Client(account_sid, auth_token)
            message = client.messages.create(
                from_='+14092455639',
                body = 'Great news! Your order is en route. It should be arriving soon.',
                to = str(user_data[str(update.message["chat"]["id"])]["customer_contact"])
            )
            print(message.sid)
            update.message.reply_text("Great, your customer will see you at the location.")
            user_data.clear()
            return ConversationHandler.END

    elif category == "Help":
        if "support_message" not in user_data[str(update.message["chat"]["id"])]:
            if text == "Go Back":
                user_data.clear()
                return ConversationHandler.END
            reply_keyboard = [['Done']]
            markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
            update.message.reply_text("Welcome to the support section. Enter a message to send to our team. Include your order ID if relevant. We will get back to you as soon as we can.", reply_markup=markup)
            user_data[str(update.message["chat"]["id"])]["support_message"] = ""
        elif "support_message" in user_data[str(update.message["chat"]["id"])]:
            if text == "Done":
                user_data.clear()
                return ConversationHandler.END
            mess = text
            user_data[str(update.message["chat"]["id"])]["support_message"] = text

            account_sid = 'AC786497ab3dd58ca05c5ad0b2096e9755'
            auth_token = '1f64b515c4f25aa8642fa617b7f7bd40'
            client = Client(account_sid, auth_token)
            message = client.messages.create(
                from_='+14092455639',
                body = 'CrowdPotato Support Message\nFrom: ' + str(update.message["chat"]["username"]) + '\nMessage:\n' + text,
                to = '+14159870628'
            )
            print(message.sid)
            update.message.reply_text("We've got your message. We'll respond as soon as we can!")
            user_data.clear()
            return ConversationHandler.END

    # update.message.reply_text("Neat! "
    #                           "{}"
    #                           "You can tell me more, or change your opinion on something.".format(
    #                               facts_to_str(user_data)), reply_markup=markup)

    return TYPING_REPLY

def done(bot, update, user_data):
    if (str(update.message["chat"]["id"])) in user_data:
        if 'topic' in user_data[str(update.message["chat"]["id"])]:
            del user_data[str(update.message["chat"]["id"])]['topic']

        update.message.reply_text("{}"
                                  "Until next time!".format(facts_to_str(user_data[str(update.message["chat"]["id"])])))

        user_data.clear()
        return ConversationHandler.END
    else:
        update.message.reply_text("Until next time!")
        user_data.clear()
        return ConversationHandler.END


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    # Create the Updater and pass it your bot's token.
    updater = Updater("636301646:AAHLqYfoCy7R0YYmZNDtDdfwfILxW1SDoVk")

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # Add conversation handler with the states CHOOSING, TYPING_CHOICE and TYPING_REPLY

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('hi', start), CommandHandler('hello', start), CommandHandler('hi there', start), CommandHandler('start', start)],

        states={
            CHOOSING: [RegexHandler('^(Order Food|Check Delivery Status|Promotions|Cancel Order|Fufill Order|Help)$',
                                    regular_choice,
                                    pass_user_data=True),
                       RegexHandler('^Something else...$',
                                    custom_choice),
                       ],

            TYPING_CHOICE: [MessageHandler(Filters.text,
                                           regular_choice,
                                           pass_user_data=True),
                            ],

            TYPING_REPLY: [MessageHandler(Filters.text,
                                          received_information,
                                          pass_user_data=True),
                           ],
        },

        fallbacks=[RegexHandler('^(Done|Cancel|Stop|Nevermind|Back)$', done, pass_user_data=True)]
    )

    dp.add_handler(conv_handler)

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()#poll_interval = 1.0,timeout=20)

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()

main()
